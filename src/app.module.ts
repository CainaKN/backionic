import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';

@Module({
  imports: [MongooseModule.forRoot('mongodb+srv://kinoquix:meteoro123@cluster0-2slin.mongodb.net/test?retryWrites=true&w=majority'),
  UserModule,
],
controllers: [],
providers: [],
})
export class AppModule {}